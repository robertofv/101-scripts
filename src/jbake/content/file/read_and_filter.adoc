= Buscar en fichero y volcar coincidentes
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-08-23
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: file
:jbake-script: /scripts/file/ReadAndFilter.groovy
:idprefix:
:imagesdir: ../images


En algunos sistemas operativos tenemos el comando _grep_ o _find_ que buscan una cadena en los
ficheros de un directorio y la vuelcan por consola. Con este script vamos a realizar algo parecido
para ver cómo podemos leer ficheros de gran tamaño y cómo escribir en otro fichero.

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> Creamos o sobreescribimos un fichero de salida y usamos un writer para escribir en él
<2> readLine es indicada para leer ficheros de gran tamaño, teniendo otras formas de leer un fichero como .text que leerían todo el fichero en una cadena
<3> utilizamos el operator leftshit para ir enviando cadenas al fichero



