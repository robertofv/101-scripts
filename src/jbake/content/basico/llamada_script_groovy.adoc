= Llamadas entre scripts
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-31
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/Configuration.groovy
:idprefix:
:imagesdir: ../images


El caso más fácil de explicar es cuando tenemos varios scripts en los cuales hay una función/acción que se repite
en cada uno de ellos y cada vez que creamos un nuevo script debemos incluir. Está acción repetitiva puede ir desde
la conexión a una base de datos, exportar a excel o simplemente contener los datos de conexión a la base de datos.

A continuación vamos a crear un script llamado `Configuration.groovy` el cual contendrá toda la infomación necesaria
para conectarnos a una base de datos de nuestro entorno. Esta configuración podrá ser llamada desde cualquier
de nuestros scripts, por lo tanto de esta manera evitaremos tener que copiar y pegar esa parte de código para como en
este caso a una base de datos de nuestro contexto.

.Configuration.groovy
[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> Método que nos devuelve usuario para conectarnos.
<2> Método que nos devuelve la contraseña.
<3> Método que nos devuelve el servidor donde nos vamos conectar.
<4> Método que nos devuelve la base de datos.
<5> Método que nos devuelve la url para conectarnos.
<6> Método que nos devuelve una instancia mysql con los datos ya definidos.
<7> Método que nos devuelve una instancia mysql con los datos enviados por parámetro.

Para obtener una instancia podemos realizarlo de la siguiente manera:

.Script.groovy
[source,groovy]
----
def sql = Configuration.instanceMysql()
----

O con los datos de conexión enviados por parámetro:

.OtroScript.groovy
[source,groovy]
----
def my_user = "user",
def my_passwd = "passwd",
def my_server ="localhost"
def database = "test"
def my_sql = Configuration.instanceMysql(my_user,my_passwd,my_server,my_database)
----


