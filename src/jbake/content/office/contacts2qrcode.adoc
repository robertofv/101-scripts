= Contacts2QRCode
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-11-03
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: office
:jbake-script: /scripts/office/Contacts2QRCode.groovy
:idprefix:
:imagesdir: ../images



En ete script vamos a tratar las siguientes capacidades de Groovy:

- Leer todas las Hojas, Filas y Columnas de un Excel
- Escribir una celda en función de los valores leídos y/o lógica de negocio
- Invocar a un servicio remoto y volcar el resultado a un fichero

== Objetivo

Contacts2QRCode es un script que partiendo de un Excel donde se hayan guardado datos de contactos de una serie de
personas, como por ejemplo los empleados de tu organización, generar para cada uno de ellos un código QRCode para
incluir en las tarjetas de visita.

:icons: font
NOTE: Un código QR (del inglés Quick Response code, "código de respuesta rápida") es la evolución del código de barras.
Es un módulo para almacenar información en una matriz de puntos o en un código de barras bidimensional.

NOTE: QRCode tiene numerosas utilidades y formatos. En este post trataremos cómo generar un VCard, formato destinado
a especificar los datos de contacto de una persona, como nombre, puesto, dirección, teléfono, etc.

NOTE: Para la generación del código QRCode utilizaremos el servicio gratuito zxing (http://zxing.org/)
{set:icons!:}

== Excel

El excel con los datos de nuestros contactos contendrá la siguiente estructura, siendo la primera fila destinada
a contener la cabecera y por ello ignorada por el script:

|===
|Id | id del empleado | se utiliza como nombre de fichero QR
|Name	| nombre del empleado | puede contener espacios y ser compuesto con apellidos, etc
|JobTitle	| puesto    | puede ser blanco
|Phone	| telefóno  | asegurate que el formato es una cadena para que no se trate como numérico
|eMail	| | puede ser blanco
|Address	| dirección en una sola línea |
|Organization	| |
|URL	    | http://xxxx.yyy/zzzz  | si disponeis de página personal para cada empleado o en la empresa
|QR     | | esta columan será escrita por el script cada vez que se invoque
|===


== Dependencias

Para la lectura del Excel utilizaremos esta vez las librerías de Apache, POI

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

== Parseo del Excel

Durante la primera pasada, el script leerá todas las hojas de cálculo y sus filas suponiendo que siguen el formato
anteriormente explicado, generando para cada fila una URL personalizada:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=readExcel]
----
<1> Leemos el excel en modo escritura
<2> Iteramos en todas las hojas del Excel. Podríamos buscar una en concreto si así lo quisieramos
<3> Iteramos por todas las filas ignorando la primera
<4> Invocamos a la función que genera el link
<5> Una vez terminado reescribimos el excel en memoria
<6> Volcamos el buffer de memoria al fichero de origen

== Dump de QRCodes

Si se especifica un segundo parámetro, este indicará el directorio donde queremos que se generen los QRCodes de
cada empleado. Realizaremos el mismo bucle de lectura que en el apartado anterior pero esta vez buscaremos la
columna que se actualizó. Dicha columna es una URL que nos devuelve un PNG con el código generado, por lo que
usaremos la sintaxis de Groovy para descargarlo directamente a un fichero:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dumpQr]
----
<1> Leemos el excel en modo lectura
<2> Gracias a Groovy *toURL()* genera una URL y *bytes* nos devuelve el contenido de la response como un array de bytes

== generateQRLink

La función generateQRLink simplemente lee los campos de la fila Excel y los concatena a la URL de *Zxing* devolviendo
la cadena resultante

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=generateQRLink]
----

== GenerateVCard

La función generateVCard a la que se hace referencia en el código anterior es una simple concatenación de _String_
en formato VCARD:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=generateVCard]
----
<1> Utilizaremos URLEncoder para conseguir que los campos con espacio, acentos etc sean convertidos correctamente


