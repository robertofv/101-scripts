@Grab('mysql:mysql-connector-java:5.1.6')// <1>
@Grab('net.sourceforge.jexcelapi:jxl:2.6.12')
@GrabConfig(systemClassLoader=true)

import jxl.*
import jxl.write.*
import groovy.sql.Sql

String filename = "informe.xls"

Sql sql = Sql.newInstance( "jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false",
        "user",
        "password",
        "com.mysql.jdbc.Driver")

WritableWorkbook workbook = Workbook.createWorkbook(new File(filename))

WritableSheet sheet = workbook.createSheet("productos", 0)

boolean first=true
sql.eachRow("select sku, description from products order by sku") { row -> // <2>

  if( first ){
    sheet.addCell (new Label (0,i,"Sku"))
    sheet.addCell ( new Label (0,i,"Product") )  // <3>
    first=false
  }

  sheet.addCell( new Label (0,i+1,"$l.sku") )  // <4>
  sheet.addCell ( new Label (1,i+1,l.description) )
    
}
workbook.write()
workbook.close()
