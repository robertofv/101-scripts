//tag::dependencies[]
@GrabConfig(systemClassLoader=true)
@Grab('mysql:mysql-connector-java:5.1.6')
@Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true)
@Grab(group='org.twitter4j', module='twitter4j-core', version='4.0.6')

import groovy.sql.Sql

import static groovyx.javafx.GroovyFX.start
import javafx.application.Platform
import javafx.scene.SnapshotParameters
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

import twitter4j.TwitterFactory;
import twitter4j.StatusUpdate
//end::dependencies[]


//tag::customize[]
String message ="""
Estamos que nos salimos!!! 
Top Ventas de nuestro catálogo. Gracias a todos por elegirnos
#groovy-script
"""

String chartTitle="Top Ventas"

int width=height=400
//end::customize[]

//tag::business[]
def data=[:]    //<1>

Sql sql = Sql.newInstance( "jdbc:mysql://localhost:3306/scripts?jdbcCompliantTruncation=false",
        "root",
        "my-secret-pw",
        "com.mysql.jdbc.Driver")

sql.eachRow("select product_name, sales from sales order by sales limit 4"){
    data[it.product_name] = it.sales as double  //<2>
}
//end::business[]

//tag::vista[]
start {
    def saveNode  //<1>
    stage(visible: true) { //<2>
        scene(fill: BLACK, width: width, height: height) {
            saveNode = tilePane() {
                pieChart(data: data, title: chartTitle) //<3>
            }
        }
    }
    //end::vista[]

    //tag::snapshot[]
    def snapshot = saveNode.snapshot(new SnapshotParameters(), null);
    BufferedImage bufferedImage = new BufferedImage(saveNode.width as int, saveNode.height as int, BufferedImage.TYPE_INT_ARGB);
    BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage)

    File file = new File('screenshot.png')
    ImageIO.write(image, "png", file )
    //end::snapshot[]

    //tag::twitter[]
    StatusUpdate status = new StatusUpdate(message) //<1>
    status.media(file ) //<2>
    TwitterFactory.singleton.updateStatus status
    //end::twitter[]

    //tag::exit[]
    Platform.exit();
    System.exit(0);
    //end::exit[]
}
