max=null

void scanDir( File dir ){
  dir.eachFile{ f->
    max = max && max.size() >= f.size() ? max : f
  }
  dir.eachDir{ d ->
    scanDir(d)
  }
}

scanDir(new File('.'))

println "El fichero mayor es: $max.path y ocupa ${max.size()} bytes"
