@Grab(group='javax.mail', module='mail', version='1.4')

import javax.mail.*
import javax.mail.internet.*
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


abstract class Mail extends Script{ //<1>

    def sendEmail(subject,text,from,to){

        def  d_host = "smtp-relay.gmail.com", //<2>
             d_port  = "587" //465,587

        def props = new Properties() //<3>
        props.put("mail.smtp.user", from)
        props.put("mail.smtp.password", 'TUPASSWORD')
        props.put("mail.smtp.host", d_host)
        props.put("mail.smtp.port", d_port)
        props.put("mail.smtp.starttls.enable","true")
        props.put("mail.smtp.auth", "false")
        props.put("mail.protocol", "smtp")


        def session = Session.getInstance(props)
        session.debug=false//<4>

        def msg = new MimeMessage(session)//<5>
        msg.text = text
        msg.subject = subject
        msg.from = new InternetAddress(from)
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to))

        Transport.send( msg ) //<6>
    }
}
