import Sql

class Configuration {

        String user     = "user"         //<1>
        String passwd   = "passwd"       //<2>
        String server   = "localhost"    //<3>
        String database = "test"         //<4>
        String url      = "jdbc:mysql://$server:3306/$database?jdbcCompliantTruncation=false"    //<5>

        def instanceMysql(){  //<6>
            return Sql.newInstance( "jdbc:mysql://$server:3306/$database?jdbcCompliantTruncation=false", "$user", "$passwd", "com.mysql.jdbc.Driver")
        }
        def instanceMysql(my_user,my_passwd,my_server,my_database){ //<7>
            return Sql.newInstance( "jdbc:mysql://$my_server:3306/$my_database?jdbcCompliantTruncation=false", "$my_user", "$my_passwd", "com.mysql.jdbc.Driver")
        }
}
